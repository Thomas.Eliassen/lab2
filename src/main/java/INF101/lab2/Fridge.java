package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	int maxSize = 20;
	
	public static void main(String[] args) {
	}
	
	ArrayList<FridgeItem> FridgeItems = new ArrayList<FridgeItem>();
	
	@Override
	public int nItemsInFridge() {
	int i = 0;
		for (@SuppressWarnings("unused") FridgeItem item : FridgeItems) {
		i++;
		}
	return i;
	}

	@Override
	public int totalSize() {
		return maxSize;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		int size = FridgeItems.size();
		if (size == maxSize)
			return false;
		else FridgeItems.add(item);
		return true;
	}
	
	@Override
	public void takeOut(FridgeItem item) {
		if(FridgeItems.contains(item)) {
			FridgeItems.remove(item);
		}
		else throw new NoSuchElementException("no such item in fridge");
	}

	@Override
	public void emptyFridge() {
		FridgeItems.removeAll(FridgeItems);
		
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		ArrayList<FridgeItem> badFood = new ArrayList<>();
		for(FridgeItem item: FridgeItems) {
			if (item.hasExpired() == true) {
				badFood.add(item);
				}
			}
		FridgeItems.removeAll(badFood);
		return badFood;
	}
	
	
}
